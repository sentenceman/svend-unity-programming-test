﻿using System;

/// <summary>
/// Class that contains an entry in the high scores, and is persisted to disk.
/// </summary>
[Serializable]
public class HighScoreEntry : IComparable<HighScoreEntry>
{
	public string playerName;
	public int score;

	public HighScoreEntry(string playerName, int score)
	{
		this.playerName = playerName;
		this.score = score;
	}

    public int CompareTo(HighScoreEntry other)
    {
        return other.score - score;
    }
}
