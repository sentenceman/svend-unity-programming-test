﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Loader doesn't currently do anything other than go to the menu.
/// If the GameStateManager wasn't blocking, it could wait for it first.
/// It could also do the instantiation of the GameStateManager object, but I've just put it in the scene instead.
/// That could be a problem is the load scene was loaded more than once, as there would be multiple game state managers instantiated.
/// </summary>
public class Loader : MonoBehaviour
{
    [SerializeField] private GameStateManager _gameStateManagerPrefab;

    [UsedImplicitly]
	private void Start()
	{
        SceneManager.LoadScene("menu");
	}

}
