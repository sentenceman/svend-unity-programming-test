﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles and blick, it's levels and its death.
/// Each level has a different colour and it goes down a level every time it's hit (by a ball).
/// When it reaches level 0 it's destroyed.
/// Being hit and destroyed triggers events that others can listen to (e.g. game controller).
/// </summary>
public class Brick : MonoBehaviour {

    [SerializeField] private int _startLevel;
    [SerializeField] private List<Color> _levelColours;
    [SerializeField] private List<PowerUp> _powerUpPrefabs;
    [SerializeField] private float _powerUpDropChance;

    public static event Action onBrickDestroyed;
    public static event Action onBrickHit;

    private SpriteRenderer _spriteRenderer;
    private int _currentLevel;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _currentLevel = _startLevel;
        _spriteRenderer.color = _levelColours[Mathf.Min(_currentLevel, _levelColours.Count) - 1];
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Balls"))
        {
            if (--_currentLevel <= 0)
            {
                if (onBrickDestroyed != null) onBrickDestroyed();
                if (UnityEngine.Random.Range(0f, 1f) < _powerUpDropChance)
                {
                    PowerUp powerUp = Instantiate(_powerUpPrefabs[UnityEngine.Random.Range(0, _powerUpPrefabs.Count)]);
                    powerUp.transform.position = transform.position;
                }
                Destroy(gameObject);
            }
            else
            {
                if (onBrickHit != null) onBrickHit();
                _spriteRenderer.color = _levelColours[Mathf.Min(_currentLevel, _levelColours.Count) - 1];
            }
        }
    }
}
