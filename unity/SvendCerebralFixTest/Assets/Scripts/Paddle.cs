﻿using UnityEngine;

public class Paddle : MonoBehaviour
{
    private Rigidbody2D _rigitbody;
    private float _currentSpeed;
    // How far should the "mouse" (or touch) x position be from the paddle, before the paddle moves
    private const float MOVE_THRESHOLD = 10f;

    private void Start()
    {
        _rigitbody = GetComponent<Rigidbody2D>();
        GameController.onPaddleSpeedChanged += OnPaddleSpeedChanged;
    }

    public void Init(float initialSpeed)
    {
        _currentSpeed = initialSpeed;
    }

    private void OnPaddleSpeedChanged(float newSpeed)
    {
        _currentSpeed = newSpeed;
    }

    private void Update()
    {
        // Move the paddle based on either keys or mouse/touch input
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            _rigitbody.velocity = new Vector2(-1, 0) * _currentSpeed;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            _rigitbody.velocity = new Vector2(1, 0) * _currentSpeed;
        }
        else if (Input.GetMouseButton(0))
        {
            Vector2 paddleScreenPosition = Camera.main.WorldToScreenPoint(transform.position);
            if (Input.mousePosition.x - paddleScreenPosition.x > MOVE_THRESHOLD)
            {
                _rigitbody.velocity = new Vector2(1, 0) * _currentSpeed;
            }
            else if (paddleScreenPosition.x - Input.mousePosition.x > MOVE_THRESHOLD)
            {
                _rigitbody.velocity = new Vector2(-1, 0) * _currentSpeed;
            }
            else
            {
                _rigitbody.velocity = Vector2.zero;
            }
        }
        else
        {
            _rigitbody.velocity = Vector2.zero;
        }
    }

}
