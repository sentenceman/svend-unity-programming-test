﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Shown when a new high score is acheived and lets the user enter their name.
/// Submits the entered name (or "Anonymous" if no name given) to the GameStateManager).
/// </summary>
public class NewHighScorePanel : MonoBehaviour
{
    [SerializeField] private Text _highScoreValue;
    [SerializeField] private InputField _highScoreName;

    private int _score;

    public void SetNewScore(int score)
    {
        _score = score;
        _highScoreValue.text = score.ToString();
    }

    public void SubmitPressed()
    {
        GameStateManager.instance.ReportNewScore(!string.IsNullOrEmpty(_highScoreName.text) ? _highScoreName.text : "Anonymous", _score);
        gameObject.SetActive(false);
    }
}
