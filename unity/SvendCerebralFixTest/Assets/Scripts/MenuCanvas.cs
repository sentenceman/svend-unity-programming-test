﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Handles the main menu. Currently it doesn't do anything, as the high score panel loads itself.
/// Should probably handle that and pass to / activate high scores view.
/// </summary>
public class MenuCanvas : MonoBehaviour
{

    private void Awake()
    {
        // If we haven't got a GameStateManager, it's because the game wasn't started from the load scene.
        // Go there now...
        if (GameStateManager.instance == null)
        {
            SceneManager.LoadScene("load");
        }
    }

    public void PlayPressed()
	{
        // Go to game.
        SceneManager.LoadScene("game");
	}
}
