﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Class for managing, persisting and loading the "state" of the game.
/// Currently the only state is the high scores.
/// </summary>
public class GameStateManager : MonoBehaviour
{
    [SerializeField] private int _highScoresCapacity;

    public static GameStateManager instance { get; private set; }

    private const string HIGH_SCORES_KEY = "highscores";

    private HighScore _highScore;

    public bool hasLoaded { private set; get; }

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
	{
        string highscoreJson = PlayerPrefs.GetString(HIGH_SCORES_KEY);
        if (highscoreJson != null)
        {
            _highScore = JsonUtility.FromJson<HighScore>(highscoreJson);
        }
        if (_highScore == null)
        {
            _highScore = new HighScore();
            _highScore.highScoreEntries = new List<HighScoreEntry>();
        }
        SceneManager.LoadScene("menu");
    }

    public List<HighScoreEntry> GetHighScores()
    {
        return _highScore.highScoreEntries;
    }

    public bool WouldBeNewHighScore(int score)
    {
        return score > 0 && (_highScore.highScoreEntries.Count < _highScoresCapacity || score > _highScore.highScoreEntries[_highScore.highScoreEntries.Count - 1].score);
    }

    public void ReportNewScore(string name, int score)
    {
        if (_highScore.highScoreEntries.Count < _highScoresCapacity)
        {
            _highScore.highScoreEntries.Add(new HighScoreEntry(name, score));
        }
        else
        {
            _highScore.highScoreEntries[_highScore.highScoreEntries.Count - 1] = new HighScoreEntry(name, score);
        }
        _highScore.highScoreEntries.Sort();
        string saveJson = JsonUtility.ToJson(_highScore);
        PlayerPrefs.SetString(HIGH_SCORES_KEY, saveJson);
        PlayerPrefs.Save();
    }
}