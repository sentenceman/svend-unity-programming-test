﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Class that manages the game over panel and contains callbacks for the "Play again" and "Exit" buttons.
/// </summary>
public class GameOverPanel : MonoBehaviour
{
    [SerializeField] private Text _finalScoreText;

    public void SetFinalScore(int score)
    {
        _finalScoreText.text = "Score: " + score;
    }

    public void PlayAgainSelected()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ExitSelected()
    {
        SceneManager.LoadScene("menu");
    }
}
