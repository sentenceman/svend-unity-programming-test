﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Class that contains the core game and handles events from the game entities.
/// Keeps track of bricks and balls "alive" in the game.
/// If there are no more bricks the level is cleared and a new level is spawned (resetting balls)
/// IF there are no more balls, it's game over.
/// Increases ball and paddle speed slightly with every level.
/// Also contains an audio source and plays sounds on certain events.
/// </summary>
public class GameController : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    [SerializeField] private List<Vector2> _brickPositions;
    [SerializeField] private Transform _brickContainer;
    [SerializeField] private Brick _brickPrefab;
    [SerializeField] private GameOverPanel _gameOverPanel;
    [SerializeField] private NewHighScorePanel _newHighScorePanel;
    [SerializeField] private Ball _ballPrefab;
    [SerializeField] private Paddle _playerPaddle;
    [SerializeField] private float _initialPaddleSpeed;
    [SerializeField] private float _initialBallSpeed;
    [SerializeField] private float _paddleSpeedIncrementPerLevel;
    [SerializeField] private float _ballSpeedIncrementPerLevel;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _brickDestroyedClip;
    [SerializeField] private AudioClip _brickHitClip;
    [SerializeField] private AudioClip _ballWallClip;
    [SerializeField] private AudioClip _ballPaddleClip;

    public static event Action<float> onPaddleSpeedChanged;
    public static event Action<float> onBallSpeedChanged;

    private int _score;
    private int _lastDisplayedScore;
    private int _remainingBricks;
    private float _currentBallSpeed;
    private float _currentPaddleSpeed;
    private int _currentLevel;
    private readonly List<Ball> _currentBalls = new List<Ball>();

    private void Awake()
    {
        if (GameStateManager.instance == null)
        {
            SceneManager.LoadScene("load");
        }
    }

    private void Start()
    {
        _score = 0;
        _lastDisplayedScore = _score;
        _scoreText.text = "Score: " + _score;
        Time.timeScale = 1;
        _gameOverPanel.gameObject.SetActive(false);
        _newHighScorePanel.gameObject.SetActive(false);
        _currentPaddleSpeed = _initialPaddleSpeed;
        _currentBallSpeed = _initialBallSpeed;
        _playerPaddle.transform.position.Set(0, _playerPaddle.transform.position.y, 0);
        _playerPaddle.Init(_currentPaddleSpeed);
        _currentLevel = 0;
        StartLevel(_currentLevel);
    }

    private void StartLevel(int level)
    {
        SpawnBricks();
        _currentBallSpeed = (1 + level * _ballSpeedIncrementPerLevel) * _initialBallSpeed;
        _currentPaddleSpeed = (1 + level * _paddleSpeedIncrementPerLevel) * _initialPaddleSpeed;
        if (onBallSpeedChanged != null) onBallSpeedChanged(_currentBallSpeed);
        if (onPaddleSpeedChanged != null) onPaddleSpeedChanged(_currentPaddleSpeed);
        _currentBalls.ForEach(ball => Destroy(ball.gameObject));
        _currentBalls.Clear();
        SpawnBall();
    }

    private void OnEnable()
    {
        Brick.onBrickDestroyed += OnBrickDestroyed;
        Brick.onBrickHit += OnBrickHit;
        Ball.onBallDestroyed += OnBallDestroyed;
        Ball.onBallHitPaddle += OnBallHitPaddle;
        Ball.onBallHitWall += OnBallHitWall;
        PowerUp.onPowerUpPickedUp += OnPowerUpPickedUp;
    }

    private void OnDisable()
    {
        Brick.onBrickDestroyed -= OnBrickDestroyed;
        Brick.onBrickHit -= OnBrickHit;
        Ball.onBallDestroyed -= OnBallDestroyed;
        Ball.onBallHitPaddle -= OnBallHitPaddle;
        Ball.onBallHitWall -= OnBallHitWall;
        PowerUp.onPowerUpPickedUp -= OnPowerUpPickedUp;
    }

    private void Update()
    {
        if (_lastDisplayedScore != _score)
        {
            _lastDisplayedScore = _score;
            _scoreText.text = "Score: " + _score.ToString();
        }

        if (_remainingBricks <= 0)
        {
            StartLevel(++_currentLevel);
        }
    }

    private void OnPowerUpPickedUp(PowerUp.Type type, AudioClip powerUpSound)
    {
        Debug.Log("Picked up powerup of type: " + type);
        switch (type)
        {
            case PowerUp.Type.ExtraBall:
                SpawnBall();
                break;
            case PowerUp.Type.PaddleSpeedUp:
                _currentPaddleSpeed *= 2;
                if (onPaddleSpeedChanged != null) onPaddleSpeedChanged(_currentPaddleSpeed);
                break;
        }
        _audioSource.PlayOneShot(powerUpSound);
    }

    private void OnBrickDestroyed()
    {
        Debug.Log("Destroyed Brick!");
        --_remainingBricks;
        ++_score;
        _audioSource.PlayOneShot(_brickDestroyedClip);
    }

    private void OnBrickHit()
    {
        _audioSource.PlayOneShot(_brickHitClip);
    }

    private void OnBallDestroyed(Ball ball)
    {
        _currentBalls.Remove(ball);
        if (_currentBalls.Count == 0)
        {
            Time.timeScale = 0;
            _gameOverPanel.SetFinalScore(_score);
            _gameOverPanel.gameObject.SetActive(true);
            if (GameStateManager.instance.WouldBeNewHighScore(_score))
            {
                _newHighScorePanel.SetNewScore(_score);
                _newHighScorePanel.gameObject.SetActive(true);
            }
        }
    }

    private void OnBallHitWall()
    {
        _audioSource.PlayOneShot(_ballWallClip);
    }

    private void OnBallHitPaddle()
    {
        _audioSource.PlayOneShot(_ballPaddleClip);
    }

    private void SpawnBricks()
    {
        foreach (Vector2 position in _brickPositions)
        {
            Brick newBrick = Instantiate(_brickPrefab);
            newBrick.transform.parent = _brickContainer;
            newBrick.transform.localScale = Vector2.one;
            newBrick.transform.localPosition = position;
        }
        _remainingBricks += _brickPositions.Count;
    }

    private void SpawnBall()
    {
        Ball ball = Instantiate(_ballPrefab);
        ball.transform.position = _playerPaddle.transform.position + new Vector3(0, 0.2f, 0);
        ball.Init(_currentBallSpeed);
        _currentBalls.Add(ball);
    }
}
