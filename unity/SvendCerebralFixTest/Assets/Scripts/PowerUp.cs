﻿using System;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    public enum Type
    {
        ExtraBall,
        PaddleSpeedUp
    }

    [SerializeField] private Type _type;
    [SerializeField] private AudioClip _powerUpSound;

    public static event Action<Type, AudioClip> onPowerUpPickedUp;

    public Type GetPowerUpType()
    {
        return _type;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Paddle"))
        {
            if (onPowerUpPickedUp != null) onPowerUpPickedUp(_type, _powerUpSound);
        }
        Destroy(gameObject);
    }
}
