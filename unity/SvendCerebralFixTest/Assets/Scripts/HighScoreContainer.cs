﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// This manages populating the high scores table from the GameStateManager's high scores.
/// </summary>
public class HighScoreContainer : MonoBehaviour
{
    [SerializeField] private List<Text> _highScoreNames;
    [SerializeField] private List<Text> _highScoreScores;

    private void Awake()
    {
        // If we haven't got a GameStateManager, it's because the game wasn't started from the load scene.
        // Go there now...
        if (GameStateManager.instance == null)
        {
            SceneManager.LoadScene("load");
        }
    }
    private void OnEnable()
    {
        if (GameStateManager.instance == null)
        {
            return;
        }

        // If we haven't got an equal number of name and score labels, it's broken.
        // I'd have likes to make the entries prefabs and instantiate them here in a list view, but did it
        // like this because it was faster.
        if (_highScoreNames.Count != _highScoreScores.Count)
        {
            Debug.LogError("Please set the same number of high score name labels and high score scores labels!");
            return;
        }
        for (int i=0; i < _highScoreNames.Count; ++i)
        {
            List<HighScoreEntry> highScoreEntries = GameStateManager.instance.GetHighScores();
            if (i < highScoreEntries.Count)
            {
                _highScoreNames[i].text = (i + 1) + ". " + highScoreEntries[i].playerName;
                _highScoreScores[i].text = highScoreEntries[i].score.ToString("n0");
            }
            else
            {
                _highScoreNames[i].text = (i + 1) + ".";
                _highScoreScores[i].text = "";
            }
        }
    }
}
