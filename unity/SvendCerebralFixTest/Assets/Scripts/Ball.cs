﻿using System;
using UnityEngine;

/// <summary>
/// Class that controls the ball. It's behaviour is mostly controlled by the physics system,
/// but a small "hack" has been added to avoid it getting "stuck" either up against a wall or going back and forth horizontally.
/// </summary>
public class Ball : MonoBehaviour {

    public static event Action<Ball> onBallDestroyed;
    public static event Action onBallHitWall;
    public static event Action onBallHitPaddle;

    private Rigidbody2D _rigidbody;
    private float _currentSpeed;

    private const float STUCK_THRESHOLD_X = 0.02f;
    private const float STUCK_THRESHOLD_Y = 0.1f;

    private void OnEnable()
    {
        GameController.onBallSpeedChanged += OnBallSpeedChanged;
    }

    private void OnDisable()
    {
        GameController.onBallSpeedChanged -= OnBallSpeedChanged;
    }

    private void Update()
    {
        // Sometimes the ball gets stuck going either completely vertical or horizontal
        // These hack corrects this when it happens
        if (Mathf.Abs(_rigidbody.velocity.x) <= STUCK_THRESHOLD_X || Mathf.Abs(_rigidbody.velocity.y) <= STUCK_THRESHOLD_Y)
        {
            float randomAngle = UnityEngine.Random.Range(-10f, 10f);
            Debug.Log("Turning stuck ball by " + randomAngle + " degrees");
            Vector2 randomDirection = Quaternion.Euler(0, 0, randomAngle) * _rigidbody.velocity;
            Vector2 randomVelocity = randomDirection.normalized * _currentSpeed;
            _rigidbody.velocity = randomVelocity;
        }
    }
    
    public void Init(float initialSpeed)
    {
        Debug.Log("Setting ball speed to: " + initialSpeed);
        _rigidbody = GetComponent<Rigidbody2D>();
        _currentSpeed = initialSpeed;
        float randomAngle = UnityEngine.Random.Range(-45f, 45f);
        Vector2 randomDirection = Quaternion.Euler(0, 0, randomAngle) * new Vector2(0, 1);
        Vector2 randomVelocity = randomDirection.normalized * _currentSpeed;
        _rigidbody.velocity = randomVelocity;
    }

    private void OnBallSpeedChanged(float newSpeed)
    {
        Debug.Log("Ball speed changed to: " + newSpeed);
        _currentSpeed = newSpeed;
        _rigidbody.velocity = _rigidbody.velocity.normalized * _currentSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
	{
        if (collision.gameObject.tag == "KillZone")
        {
            if (onBallDestroyed != null) onBallDestroyed(this);
            Destroy(gameObject);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Walls"))
        {
            if (onBallHitWall != null) onBallHitWall();
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Paddle"))
        {
            if (onBallHitPaddle != null) onBallHitPaddle();
        }
    }
}
