﻿using System;
using System.Collections.Generic;

/// <summary>
/// Class for serialising the high scores (as a List<..> can't be serialised by JsonUtility directly.
/// </summary>
[Serializable]
public class HighScore
{
    public List<HighScoreEntry> highScoreEntries;
}
